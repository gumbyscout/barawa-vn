layeredimage barawa:
  always:
    "barawa_base"

  group face:
    attribute normal default:
      "barawa_normal"
    attribute wink:
      "barawa_wink"
    attribute sad:
      "barawa_sad"
    attribute fear:
      "barawa_fear"
    attribute angry:
      "barawa_angry"
    attribute laugh:
      "barawa_laugh"
    attribute puppyeyes:
      "barawa_puppy_eyes"
    attribute serious:
      "barawa_serious"
    attribute surprise:
      "barawa_surprise"

define b = Character("Barawa", image="barawa")