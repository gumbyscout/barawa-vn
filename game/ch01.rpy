label start:
    scene bg grandcypher dining
    with fade

    show barawa
    with dissolve

    b laugh "Ah, captain! I’d been looking all over for you. I was wondering if I could have your input on…"

    show barawa serious

    """
    Barawa’s sentence trails off once he realizes the current condition of the captain.

    Their shoulders are slumped and their gaze is focused to a wall on the other side of the table.
    """

    show barawa surprise

    """
    It doesn’t seem like they’ve taken notice of him despite his boisterous voice.
    """

    # TODO: an inquisitive expression?

    """
    Curiously Barawa approaches them and leans in to peer at them.
    """

    b "Captain? Ooooh Captain? are you there?"

    "He taps on the table to try and get their attention and still get no response."

    # TODO: this should be more of a worry

    show barawa fear

    "Persistent, he leans in closer to speak while resting a hand on their shoulder and speaks into their ear in a low tone of voice as not to startle them."

    b "Can you hear me? Are you feeling unwell?"

    "The Captain jumps with a start, reaching a hand for what Barawa could only assume was a weapon, before Barawa stops them by grasping at their wrist." with vpunch

    b laugh "Woah there, it’s just me!"

    "He laughs a little bit when he sees that look of relief on the Captain’s face."

    b serious "I don’t often see you lost in thought, nor do I think you would pass time by staring into space. You got something on your mind?"

    "Barawa pulls out the chair next to the Captain and sits down beside them. For a moment, the Captain hesitates to tell him anything."

    b laugh "Oh come now, can’t expect us to sit here forever. Rely on your ol’ friend Barawa to help! I am a detective after all- maybe I can help you find a solution to your problems."

    """
    There is a moment of silence before the captain explains their feelings of anguish.
    
    To paint a picture, the captain is experiencing what can be called an existential crisis.
    """

    show barawa sad

    "They don’t believe in themself, think that they aren’t cut out for the job, are thinking of quitting and are afraid of letting everyone down."
    "The captain overall needs more encouragement but has only kept their thoughts to themself."

    b serious "...I see."

    "Intently, Barawa listens before clapping his hands together once and standing up abruptly." with vpunch

    b laugh "Well then! I believe this calls for a day out in town! Come on, captain, let your good friend Barawa show you a good time."

    "The captain looks appalled at the notion after having essentially spilled their heart out to him and looks like they want to say something, but Barawa shooshes them by putting a hand up to their lips."

    # TODO: worried smile

    b sad "Shh… just play along with me, alright?"

    # TODO: closed mouth closed eyes smile. Also fix dialog lines
    
    b laugh "I promise to address your situation after we go out for a bit, alright?"

    show barawa laugh

    "With that Barawa smiles and before the captain has a chance to protest, he wraps an arm around their waist and pulls them out of the living room and to wherever the detective has planned for the two of them."

    scene bg grandcypher exterior
    with fade
    show barawa 
    with dissolve

    """
    Pulling the captain along with him, Barawa leads them onto the ship’s deck to take in the scenery.

    He sets the captain down by the deck’s railing, the skies still a brilliant shade of blue to show that morning had broken and there was still a long way to go before the end of the day.

    The crew is currently docked at the auguste isles for the time being to stretch their legs and take a break from travelling, so the presence of other members is scarce.
    """

    b laugh "Ah, nothing beats a spot of fresh air! Isn’t that right, Captain?"

    # TODO: show view instead of barawa

    "The Captain’s posture shows that they are a little uncomfortable. They seem reluctant to admire the view to the same extent as Barawa."

    # TODO: smiling with eye brows raised

    b sad "Hey now, don’t make a face like that."

    show barawa serious

    "Barawa takes the Captain’s face into his hands and squishes their cheeks with the palms of his hands."

    b "I told you to trust me, right? So just relax and play along with me."

    # TODO: fix voiced dialog 
    
    b normal "Now then… I want you to practice your breathing with me, okay?"

    b wink "No more agonizing by yourself when I’m around!"
    
    # TODO: chuffed expression here

    b chuffed "Now, I bet you haven’t had a chance to admire the sites in Auguste while you’re travelling, right? All that excitement from the Erste empire must have kept you busy."

    "The Captain flinches in response, quite nervous by the mention of the empire."

    # TODO: chuffed

    b chuffed "Now, now. I’m not holding that against you. I’m just saying… you never really got to admire the island for what it has to offer."

    b chuffed "So, in order to express my gratitude to you for all you have done for me thus far.."

    # TODO: fixed voiced dialog

    b laugh "I think now is the best opportunity to take you on the tour of a lifetime!"

    # The Captain contemplates his suggestion and gives him a response of uncertainty.

    menu:
        "I'm not so sure...":
            pass

    b serious "And remember, while you’re here with me, you better not start stressing out about the crew."

    b sad "Your cohorts Katalina and Rackam can hold down the fort while you’re away…"

    # TODO: fixed voiced dialog
    
    b wink "Besides, I think they’re just as worried about you as I am."

    "Barawa winks at the Captain before drapes an arm around them as he leads them off of the ship."

    b laugh "Now then, I believe it’s time for our first destination!"

    jump ch02