label ch04A:
  scene bg venera resort
  show barawa laugh
  with fade

  b "...Venera Beach!"

  """
  Barawa stood in front of the Captain with his arms extended out wide. The grin on his face is radiant as he stands proudly in front of the beach behind him.

  The Captain notes that there doesn’t seem to be any occupants. They were quick to note this out loud, and it made Barawa break out in a hefty laugh.
  """

  b "Bahaha! Of course there isn’t- I made sure of that. I figured you could rest easily if there were less people around you. And judging by our past with this beach… "

  b wink "Well, let’s just say I’m not trying to make you feel like you have to be on the lookout for danger."

  """
  The Captain took in his radiant smile and smiled softly to themself. They felt blessed to have a crew member that thought about them.
  """

  show barawa blush
  "Taking in the Captain’s expression, Barawa blushes a bit in embarrassment, not expecting to see such a soft expression on the Captain’s face."

  b "W-Well, should we get going then? The day isn’t getting any younger, and there’s still so much I’d like to do for you."

  """
  Making his way over to the Captain, Barawa takes the Captain’s hand in his and leads him down to the part of the beach where a lone shack sits along the shoreline. There well all manner of food scattered neatly upon a wooden table with a grill on standby.

  Curious, the Captain motions to all the ingredients in front of them.
  """

  b normal "What we had at the cafe wasn’t exactly a meal, so I thought I’d treat you to some fresh, piping hot food- personally grilled by yours truly!"

  "He ushers the Captain over to a tent nearby that happened to act as a changing station and points at the bag sitting by the foot of the tent."

  b wink "I took the liberty of preparing a change of clothes for you, so go ahead and get dressed while I get the food ready for you! I promise you won’t be disappointed."


  hide barawa
  with moveoutright

  "And with that, the Captain was shoved into the changing station as Barawa waltzed away to attend to his ‘business’. Surely it would be okay to leave him alone, right?"

  "…Right?"

  scene black
  with fade

  centered "A FEW MINUTES LATER…"

  show bg venera resort
  show barawa serious
  with fade


  """
  When the Captain finally emerged from the changing station, they were met with an interesting sight- Barawa changed out of his usual attire and donned a pair of swim trunks and an apron over his body as he struggled to properly grill a handful of skewers. 
  
  His face looked to be sweating from concentration as he carefully timed when to turn the skewers over.

  It didn’t look like he noticed the Captain approach the shack as he leapt back from the crackling grill. 
  """

  b fear "Yikes! Was grilling always this difficult? How did those Dragon Knight fellows do this again…"

  "The Captain slowly lowered themself into their seat and it made a light creaking sound as their back hit the chair. Startled, Barawa turned in their direction and smiled sheepishly."

  b surprise "O-Oh, Captain! I see you’ve finished getting dressed. Just wait a moment- I’m almost done with this first batch of skewers."

  show barawa serious

  "His sweat seemed to increase now that he had a single audience member. The Captain offered to help but he shook his head furiously."

  b "No-no! I promised you a stress-free day off and I am going to give you one! Just sit right there and watch me work my magic!"

  """
  And the Captain did… until he saw how clumsy the Detective was with the grill. Every time he almost dropped something, the Captain would leap out of their seat to try and help them… only to be shut down by Barawa. 
  
  Was he always this stubborn?

  And so they sat there for a few minutes before finally giving up and walked over to try and help the poor man. 
  """
  
  show barawa surprise

  "Shocked, Barawa tried to push the Captain back but the Captain persisted. Reaching a hand over to touch his forearm, the Captain lightly pushed him to the side and asked him to let them help."

  b sad "But Captain- what’s that? ‘It’s more stressful watching me grill’... I see. Sorry about that Captain. I was supposed to be helping you relax, but all I’m doing is making you worry."

  "He looks rather disheartened and it makes the Captain’s heart wrench a little bit. In response, the Captain leans their head against Barawa’s large shoulder as they take a few skewers to grill for themself."

  menu:
    "Being able to spend time with you is more than enough. Thank you.":
      pass

  show barawa laugh

  "Barawa blushes at this and laughs a little to himself."

  b "Boy Captain, you sure know how to make a man feel better! I’ll be sure to make the remainder of this day a time to remember… you know, as thanks."

  scene black
  with fade

  centered "And so the two of them spent the rest of their afternoon grilling skewers while laughing, practically connected by the hip."

  jump ch05

label ch04B:
  scene bg benthic island
  show barawa laugh
  with fade

  b "...Benthic Island!"

  """
  Somewhere during their transport from Barawa’s Office to the island, Barawa had the time to don a fishing hat and swimming gear to accompany it. 
  
  He looked a bit silly wearing just a pair of swim trunks and a fishing vest… But it made him look charming in a cute way. Not that the Captain would say that out loud. 

  Just by looking at this attire, the Captain could guess what they were going to be doing.
  """

  b normal "We’re going fishing today- though it’s mostly just an excuse to spend the day on a beach."

  show barawa wink

  "Barawa gave the Captain a quick wink before ushering them over to a boat."

  hide barawa
  with moveoutleft

  scene bg benthic island boat
  with fade

  """
  It looked like he had this outing thoroughly planned out since it looked personally selected for the two of them. 
  
  The boat looked like it was high quality, and it even had a private room on the inside should the occupants of said boat felt the need to get away from the salty sea air.
  """

  show barawa laugh
  with fade

  b "I know this doesn’t look like it’ll be much fun, but doesn’t it excite you just thinking about what sort of fish we’ll catch?"

  """
  The Captain couldn’t deny that they were a little bit excited- especially since the childish look of excitement that Barawa had on his face made them feel all warm and fuzzy on the inside. 
  
  Without wasting any time to spare, the two of them hopped onto the boat and embarked on their little fishing adventure.
  """

  scene black
  with fade

  centered "A FEW MINUTES LATER…"

  scene bg benthic island boat
  show barawa serious
  with fade

  "Now decked out in swimwear themself, the Captain leaned against the railing of the boat as Barawa drove out into the open water to prepare for their first catch of the day."

  b "This should be a good spot to get started. Why don’t you get the fishing rods ready while I scout out for any fish- What’s that? You don’t know how to prep a fishing rod?"

  "The Captain shook their head, ashamed that they didn’t know how, in spite of all their adventures to the Auguste Isles."

  b @ wink "Oh, don’t be like that. It’s okay, I’ll teach  you! I happen to be a fishing expert."

  """
  Barawa moves to stand next to them, showing the Captain how to line the fishing rod, how to attach the bait, and so on. 
  
  All the while, every time he demonstrated something he would brush against the Captain’s hand to help them get accustomed to each part of the rod. 
  
  The Captain hid their face that had a blush slowly growing on their cheeks as they tried to focus on Barawa's little ‘lesson’.
  """

  b "...Aaaand there we have it! The fishing rod is ready for the day- "

  b normal @ surprise "Captain are you okay? Oooooh Captain?"

  """
  Barawa snaps his fingers in front of the Captain’s face to try and grab their attention.

  …This was gonna be a long day.
  """

  scene black
  with fade

  centered "A FEW CAPTURED FISHES AND FAILED ATTEMPTS LATER"

  scene bg benthic island boat
  show barawa laugh
  with fade

  "The Captain reels in another decent-sized catch as Barawa cheers them on."

  b "Way to go, Captain! You sure are getting the hang of this."

  """
  Barawa tosses both of their catches into the bucket they had set aside for the fishes before casting out another line. The Captain did the same, a bit satisfied by how quickly they were catching on.

  They didn’t have much time to be proud of themself, however, when a sharp tug pulled on their line.
  """

  menu:
    "...?!":
      pass

  b fear "What’s wrong, Captain? Do you have a big one on your line?"

  "He glances over at their line to see what was tugging on it… and would you look at that: It was a Bonito!"

  b surprise "Holy cow- Captain, you’ve got a Bonito on your hands! Here, let me help you try and reel it in."

  "He quickly dropped his fishing rod to move behind the Captain and wrapped his hands around theirs to aid in reeling the Bonito in. It was a desperate fight as the Bonito tugged and tugged as hard as it could to capture the bait."

  b serious "This things still got a lot of fighting spirit left! I don’t know if we’ll be able to pull it aboard, Captain-"

  show barawa surprise
  """
  Before he could finish his sentence, Barawa slipped and fell back onto the deck, taking the Captain with them. With the loss of force, the gargantuan fish pulled the fishing rod with it and swam away in triumph.

  The Captain closed their eyes to prepare for a hard impact, but hit something warm and soft instead. 
  """

  show barawa laugh
  "Carefully opening their eyes, the Captain found that they had landed onto Barawa’s broad chest. Barawa laughed as he moved to sit up with the Captain sitting in between his legs. He kept them close to him as he looked them over to make sure they weren’t injured."

  b serious "Sorry about that, Captain- Didn’t mean to lose my footing there. You’re not hurt, are you?"

  "Barawa’s hand carefully grasped the Captain’s face as he tilted it to see if they were in any pain. He smiles a bit to see that that wasn’t the case."

  b "Good, it doesn’t look like you’re feeling anything- though, your face is kind of red… are you sure you’re not hurt?"

  "The Captain quickly shook their head to deny it, hiding their embarrassment as Barawa chuckled to himself."

  b wink "Well, if you say so. We may not have caught anything to write home about, but at least I hope you had some fun?"

  "The Captain nodded happily as they leaned back against Barawa’s chest."

  scene black
  with fade
  
  centered "The two of them spent the next few moments sitting there to relish in the excitement of almost catching a Bonito during their little outing. As soon as they felt they’d had enough, the sun began to set, signaling that it was time to go."

  jump ch05