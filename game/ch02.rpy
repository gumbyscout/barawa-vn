label ch02:
    scene bg auguste cafe exterior
    with fade
    show barawa
    with dissolve

    b chuffed "I don’t think our ol’ Eugen has brought you to any cafes, now has he?"

    "The Captain shakes their head. "

    # TODO: fix voiced dialog

    b laugh "I thought so. Not much for the laid-back scene now is he? Loud places with a lot of good food sure suits him better!"

    # TODO: move the guffaw to this line

    "Barawa lets out a boisterous laugh. " with hpunch

    b chuffed "The exciting life of a captain is already loud enough as it is- take the time to relax here and let a load off."

    scene bg auguste cafe
    with fade
    show barawa laugh
    with dissolve

    "Barawa ushers the Captain inside of the cafe as he greets the cafe go-ers with a broad smile."

    b chuffed "I happen to be a regular here, and whenever I’m in the area I like to stop here whenever I need to relax."

    b wink "So the next time you’re feeling stressed, don’t hesitate to come here for a change of pace. In fact, I might come with you, too!"

    # TODO: make barawa faded out i.e. inactive here or suggested macro CG esque thing

    "Finding a secluded place within the café, Barawa leads the Captain to a place where the two of them can be alone- and so the Captain can feel at ease."

    b serious "Now you just stay here while I go and get us something to drink. Are you fine with coffee?"

    "The Captain nods in response, telling him how they like to take it."

    # TODO: shocked/surprised expression

    b surprise "Oh? I never thought you’d take it like that…"

    # TODO: fix voiced lines

    b laugh "Looks like I learned something new about you today!"

    # TODO: move laugh to the narrated section.

    "He laughs as the Captain ducks their head in embarrassment."

    b wink "I’m just teasing you. I’ll be right back, so just wait here until I come back."  

    hide barawa
    with dissolve

    # TODO: make the background desaturated or put a blacked overlay

    """
    And so, he wanders off to go place their orders. 
    
    A few moments later, Barawa returns with two cups of coffee, and to the Captain’s surprise some pastries.
    """    

    show barawa
    with dissolve
    b wink "Can’t have some coffee without something to go down the hatch with it."

    "The Captain opens their mouth to argue, but he puts up a hand to stop them."

    b laugh "Don’t try it! This is my treat. I did promise to show you a good time, after all! So just take this as an act of kindness, won’t you?"

    "When Barawa smiles at the Captain like that, there is no way that they can say no."


    # TODO: fixed voice dialog

    b wink "That’s more like it. Here, eat before they get cold."

    """
    The Captain takes a sip of their coffee and lets out a sigh of relief. 
    
    The two of them sit in silence for a few minutes before Barawa starts to speak again.
    """

    # TODO: 'surely it isn't just one...' voiced line is missing

    b serious "So tell me, what’s been weighing on your mind? Surely it isn’t just one thing that’s been holding you down."

    """
    They hesitate a bit before explaining that they underestimated the stress being a captain could hold.

    They recount the times where, during their search for their father, they have experienced world-ending events, put their own life on the line countless times, and other traumatic moments that would have led anyone else astray.

    There is a stark silence as the Captain lets out the last of their worries.

    Barawa had been nodding along to each explanation and story. His silence worries the Captain and they hang their head down low to avoid eye contact.

    In a swift movement, however, Barawa reaches a hand out to pat the Captain on the head.
    """

    b "I can’t say that I understand where you’re coming from. You’ve done everything you can for the sake of the skies and I can’t thank you enough for that."


    # TODO: missing voiced dialog

    b "But from now on, I want you to start thinking about yourself."

    "Barawa retracts his hand and continues to speak."

    b "I’m not telling you to be selfish… but I’m telling you to cherish yourself and acknowledge the work that you’ve done. You’ve made it this far, you’ve survived, and now you’re here. Anyone that has accomplished the amount of work you’ve done deserves a peace of mind."

    show barawa laugh
    "Barawa leans back in his chair and smiles at the Captain."

    b "You deserve to be satisfied with yourself, Captain."

    "The Captain can’t hold back their tears and for the next few moments, the two of them share a heartfelt moment with Barawa offering them his hand to hold for support."

    b chuffed "I have a totally different view of you, Captain! Instead of the ‘big-and-scary’ fabled Captain of the Grandcypher, you’re just like anyone else. And I’m honored to be the one that you’ll be relying on."

    "Barawa lets out a hearty laugh."

    b wink "Next time anyone causes trouble for you, just give me a holler and I’ll come running! Having me show you around the area doesn’t have to be a one-time thing after all."

    "He winks at the Captain before reaching over to ruffle their hair."

    # TODO: fix voiced dialogue

    b laugh "I think it’s about time we move on to our next destination."

    # "The Captain looks up at him with a questioning look."

    menu:
        "?...":
            pass

    b chuffed "This isn’t the only stop on our agenda! I promised to show you a good time, and I plan to follow through with that promise! So hurry and finish your coffee, the day is just getting started!"

    jump ch03