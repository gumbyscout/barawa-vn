label ch03:
  scene bg auguste office exterior
  with fade

  "To the Captain’s surprise, they find themself standing in front of one of Barawa’s many offices across the Phatagrande Skydome."

  "Not that the Captain had any complaints in the matter, but it was confusing as to why the proclaimed ‘Great Detective’ thought this would be a perfect place to relax."

  show barawa laugh
  with moveinright
  b "Ha-hah, I knew I’d surprise you with this one."

  show barawa normal
  "The Captain looks over at Barawa in confusion and asks if he was supposed to work today."

  "A worried expression falls over their face at the mere thought of interrupting his busy schedule just because they were feeling down."

  show barawa serious
  "Before they have a chance to apologize, Barawa reaches over and pats their head. His hand, much bigger than expected, felt warm and comforting and melted their worries away."

  b "Now, now. Don’t get all depressed on me. I took you here because there’s something I want to give you. It just happens to be in my office."

  b laugh "But don’t worry, We won’t be here for very long. The day is still young and I have a long list of plans for the two of us to get to."

  show barawa wink

  "He winks at them before waltzing into the building, a flustered Captain hurriedly following behind him."

  hide barawa
  with moveoutleft

  scene bg auguste office
  with fade

  show barawa
  with moveinright

  "The Captain had never had a chance to take in their surroundings whenever they were invited into Barawa’s office."

  "Most of the time, the two of them were involved in a high-stress incident, so they were too preoccupied with resolving the issue."

  "His office was much more clean than they thought it would be."

  "In the Captain’s head, they had a suspicion that the person keeping it clean was Barawa’s right hand, Sarya."

  b "Well Captain, since I’ve been making all the decisions for our little ‘outing’ today, I don’t suppose you’d like to… humor me, and make the decision for our next destination this time."

  show barawa wink

  "Barawa winks at the Captain before holding out both of his fists in front of him for the Captain to choose from."

  "He seems to be hiding something inside of them, leaving the Captain a bit curious as to what he had in store."

  menu location_choice:
    "The expression sitting on the detective’s face gave them the idea that they would have all the answers they were looking for as long as they pointed to one of his fists."

    "left fist":

      show barawa laugh

      "A grin finds its way onto Barawa’s lips as the Captain finally pointed at his left fist."

      "Turning it over, his hand opens to reveal a luminescent seashell sitting in the palm of his hand and hands it to the Captain."

      b normal "Good choice, Captain! Looks like our next destination will be…"

      jump ch04A

    "right fist":

      show barawa laugh

      "A twinkle shines from Barawa’s eye as he opens up his fist to reveal a fishing bobbin."

      b "I was hoping you’d choose this one… I’ve got a little something ‘fun’ planned for the two of us at this location… because our next destination will be…!"

      jump ch04B