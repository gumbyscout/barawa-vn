label ch05:
  scene bg auguste beach
  with fade

  """
  In the horizon, the sun seems to set over Auguste’s skyline. At the sight of the setting sun, Barawa decides that it’s time for them to begin winding down for the day. 
  
  Taking the Captain’s hand in his, he leads them to a secluded part of the beach where their own private cottage sat along the beach.
  """

  scene bg auguste room
  show barawa wink
  with fade

  b "I don’t suppose you’d be willing to have a little drink with me, eh Captain?"

  b laugh @ surprise "Oh, but… you might still be too young for that huh?"

  """
  Barawa jokes with the Captain, referring to a time in the past where he’d offered a drink to them before they had become of drinking age. 
  
  In response, the Captain lets out a laugh and playfully pushes at Barawa’s arm.
  """

  b "Haha! I’m just kidding with you. It’s about time- I’ve been looking for the day where I can finally pour a cup for you."

  b wink "I won’t give you a lot since we’ve still got a whole day ahead of us, but at least that’ll help you relax a little, no?"

  """
  The smile he gives the Captain is hypnotizing. With a nod, the Captain takes the glass cup from him and sniffs at it. The scent of the drink isn’t particularly strong and is fragrant. Barawa must have gotten one that wouldn’t be too strong for them to handle.

  The tolerance level of a Draph is said to be much larger than that of a Human’s, so to see that the detective was being considerate for their sake made the Captain feel all warm and fuzzy inside.

  As the Captain sunk back into their seat, a long awaited feeling of contentment washed over them as warmth from the alcohol set in. For the first time in months, the Captain was able to let go of any prior worries and just revel in the present moment.

  No more stress.

  No more anxiety.

  No more doubts.

  There was Barawa and his grand scheme to thank for that.
  """
b laugh "Well would you look at that! I don’t think I’ve ever seen you with a face like that, Captain."

menu:
  "What do you mean by that?":
    pass

b normal "I meant, I’ve never seen you with such a serene smile. It’s like you’ve found your inner ‘zen’, as Jin would put it. I take it that this ‘Operation’ has been a success?"

menu:
  "Deny it":
    jump operation_success_denial
  "Agree":
    jump operation_success_agree


label operation_success_denial:
  show barawa laugh

  """
  Barawa laughs at the Captain’s obvious embarrassment. Even as they turned their head to hide the blush on their face, he could tell that he ‘hit the nail on the head,’ so to speak.
  """

  b "Bahaha! You don’t have to tell me, straight. I can already tell that you’ve changed from our little outing."

  "There is only the clinking of ice against his glass cup as he takes another swig."

  b normal "But I must say, I am relieved. I’ve been paying very close attention to you the entire time, and I was worried this would’ve turned out to be a bust. You haven’t been very open about your discomfort or uncertainty, even though I was only bringing you to familiar locations."

  # The Captain can only remain silent, unable to deny Barawa’s words.

  menu:
    "...":
      pass

  b wink "Don’t worry, I’m used to my plans falling through. That’s what makes being a detective so exciting - even if I fail, there’s always another plan that’ll crack the case!"

  show barawa normal

  """
  His warm smile never leaves his face as he gets up from his spot directly across the Captain, only to settle down in the spot next to them. 

  He reaches over to rest his arm on the back of the couch directly behind the Captain’s head, his massive size dwarfing the Captain in comparison.
  """

  b "I gotta say Captain… Even though this outing was technically supposed to be for your sake, I’m glad I got to spend the day with you."

  """
  The Captain lifts their head up to look at him and smiles warmly at him as they nod in agreement, an unexpected sight that flusters Barwawa a little bit as he takes another sip of his drink.
  """

  b blush "H-Hey now, you can’t just do that out of the blue. Ol’ Barawa here isn’t used to that sort of thing…"


  # TODO: fade out in the middle of this narration

  # TODO: would make a nice CG

  """
  The room is filled with laughter as the Captain leans their body in to nestle into Barawa’s side. 

  With the day finally lifting a heavy weight off of their shoulders, the Captain’s eyes begin to slowly droop closed as they drift off into a long awaited slumber. 

  As their surroundings fade away, they can slightly make out a few parting words from their companion as the world of dreams takes them.
  """

  b normal "Sweet dreams, Captain. I’ll be waiting for you when you wake up."

  jump the_end


label operation_success_agree:
  """
  The Captain expresses their gratitude physically by getting up from their seat and walking in front of Barawa. 
  
  Before he can react, the Captain wraps their arms around him. Because of the difference in their stature, their attempted hug only reaches around the crevices of his shoulders. 
  """

  # TODO: would make a good CG point

  """
  Barawa looks a little baffled at the sudden action, taken aback by the physical display of affection quite unlike the Captain he knew that led the beloved crew aboard the Grandcypher. 
  
  Regardless, he returns the gesture by setting his glass down on the side table and brings his own arms around the Captain’s smaller frame. 

  The Captain feels almost delicate in his embrace, if not for the muscles that adorned their body from years of skyfaring experience.
  """

  b blush "Captain… I must say, this is quite unlike you. Not that I’m complaining!"

  "There is a smile on his face as he revels in the Captain’s warmth. With their body so close to his, he can make out the familiar scent of sea salt and the Captain’s natural musk. "

  b wink "I wouldn’t mind if you’d like to stay like this for the rest of the night, but I’m sure you’d like to catch up on some much needed rest after today’s events, right?"

  """
  When the Captain doesn’t answer, he starts to pull away only to be brought in closer. 
  
  Mumbling can be heard as the Captain moves closer to Barawa to sit in his lap, their face buried into the crevice of his neck. He attributed their lack of response to the drowsiness starting to set in.

  Once the Captain has settled in comfortably, he readjusts his hold on them to cradle them in his arms. 
  """

  # TODO: surprise expression is a bit too overboard and negative here

  b surprise "Wow, you must be more tired than I thought. And here I was expecting to have more of an open conversation with you too."

  show barawa laugh

  "Barawa laughs to himself."

  b wink "But given your current state, I’d say that this ‘Operation’ has been a huge success."

  """
  The Captain doesn’t speak, their soft breathing being an indication that they’ve finally drifted off to sleep. 
  
  When he can ascertain that no sign of discomfort is evident on their face, Barawa lets out a sigh of relief. 
  
  Looks like his ‘Operation: Stop the Captain from Having a Bad Day’ was a success after all.

  He makes a mental note to mark this event as his greatest accomplishment as the 'Great Detective Barawa', something that would no doubt be a source of envy from the other crew members of the Grandcypher.  

  Already anticipating a horde of angry fans at the helm of the airship, Barawa makes sure to engrain this moment deep into his memory as he ends the day with a few parting words. 
  """

  b normal "Captain, this is the most precious day I’ve ever spent with you… and here’s to many more soon to come."

  jump the_end


  label the_end:
    "THE END"